let batu = document.getElementById("batu");
let kertas = document.getElementById("kertas");
let gunting = document.getElementById("gunting");
let option = ["batu","kertas","gunting"];
let active = document.getElementsByClassName("active");
let text = document.getElementById("vs"); 
let pscore = document.getElementById("playerscore");
let cscore = document.getElementById("comscore");
let resettext = 'apalagi';
let resetbtn = document.getElementById("refresh");
let yes = document.getElementById("yes");
let no = document.getElementById("no");
let aronde = document.getElementById("angkaronde");

batu.addEventListener("click", function (e) {
    if(active.length < 2) {
        batu.classList.add("active");
        kertas.classList.remove("active");
        gunting.classList.remove("active");
        suit('batu')
    }
})

kertas.addEventListener("click", function (e) {
    if(active.length < 2) {
        kertas.classList.add("active");
        batu.classList.remove("active");
        gunting.classList.remove("active");
        suit('kertas')
    }
    
})

gunting.addEventListener("click", function (e) {
    if(active.length <2) {
        gunting.classList.add("active");
        batu.classList.remove("active");21
        kertas.classList.remove("active");
        suit('gunting')
    }
})
        
function suit(choice) {
    if (active.length < 2) {
        let pchoice = choice;
    
        let randomchoice = Math.floor(Math.random() * 3);
        let cchoice = option[randomchoice];


        option.forEach((item) => {
            let comChoiceClearElement = document.getElementById(`${item}-comp`);
            comChoiceClearElement.classList.remove("active");
        })
    
        let comChoiceElement = document.getElementById(`${cchoice}-comp`);
        comChoiceElement.classList.add("active");

        let result = check(pchoice, cchoice);
    
        if (result === 'com win') {
            text.style.backgroundColor = "lightgreen";
            text.style.width = "210px";
            text.style.height = "140px";
            text.style.transform = "rotate(-20deg)";
            text.style.textAlign = "center";
            text.style.justifyContent = "center";
            text.style.padding = "10px";
            text.style.position = "fixed";
            let textComScore = parseInt(cscore.textContent);
            cscore.textContent = textComScore + 1;
        }
    
        if (result === 'player 1 win') {
            text.style.backgroundColor = "lightgreen";
            text.style.width = "210px";
            text.style.height = "140px";
            text.style.transform = "rotate(-20deg)";
            text.style.textAlign = "center";
            text.style.justifyContent = "center";
            text.style.padding = "10px";
            text.style.position = "fixed";
            let textPlayerScore = parseInt(pscore.textContent);
            pscore.textContent = textPlayerScore + 1;
        }
        
        if (result === 'draw') {
            text.style.padding = "40px";
            text.style.backgroundColor = "green";
            text.style.width = "210px";
            text.style.height = "140px";
            text.style.transform = "rotate(-20deg)";
            text.style.textAlign = "center";
            text.style.justifyContent = "center";
            text.style.position = "fixed";
        }
      text.textContent = result;
    }
  }
  
    // alert(`player = ${pchoice} vs com = ${cchoice} pemenang = ${result} totalscore = ${totalscore}`); 

    function check(pchoice,cchoice) {
        if (pchoice === cchoice) return 'draw';
    
        if (pchoice === 'batu' && cchoice === 'kertas') {

            return 'com win';
        }
    
        if (pchoice === 'batu' && cchoice === 'gunting') {
            return 'player 1 win';
        }
    
        if (pchoice === 'kertas' && cchoice === 'batu') {
            return 'player 1 win';
        }
    
        if (pchoice === 'kertas' && cchoice === 'gunting') {
            return 'com win';
        }
    
        if (pchoice === 'gunting' && cchoice === 'batu') {
            return 'com win';
        }
    
        if (pchoice === 'gunting' && cchoice === 'kertas') {
            return 'player 1 win';
        }
    
    }
    
resetbtn.addEventListener("click", function () {
    reset();
})

function reset() {
    text.textContent = 'VS';
    text.style.backgroundColor = "";
    text.style.transform = "";
    text.style.textDecoration = "";
    text.style.padding = "";
    text.style.position = "fixed";
    text.style.top = "450px";
    gunting.classList.remove("active");
    batu.classList.remove("active");
    kertas.classList.remove("active");
    option.forEach((item) => {
        let comChoiceClearElement = document.getElementById(`${item}-comp`);
        comChoiceClearElement.classList.remove("active");
      })
        let textronde = parseInt(aronde.textContent);
            aronde.textContent = textronde + 1;
}     

